{
  "navigation": {
    "title": "JIRA Software Cloud",
    "name": "jswcloud",
    "url": "/cloud/jira/software/",
    "categories": [
      {
        "name": "devguide",
        "title": "Guides",
        "url": "/cloud/jira/software/",
        "subcategories": [
          {
            "name": "intro",
            "title": "Introduction and basics",
            "expandAlways": true,
            "items": [
              {
                "title": "Integrating with JIRA Software Cloud",
                "url": "/cloud/jira/software"
              },
              {
                "title": "Getting started",
                "url": "/cloud/jira/software/getting-started"
              },
              {
                "title": "Architecture overview",
                "url": "/cloud/jira/software/architecture-overview"
              },
              {
                "title": "Frameworks and tools",
                "url": "/cloud/jira/software/frameworks-and-tools"
              }
            ]
          },
          {
            "name": "security",
            "title": "Security",
            "items": [
              {
                "title": "Security overview",
                "url": "/cloud/jira/software/security-overview"
              },
              {
                "title": "Understanding JWT for add-ons",
                "url": "/cloud/jira/software/understanding-jwt"
              },
              {
                "isGroup": true,
                "title": "Authentication for add-ons",
                "groupItems": [
                  {
                    "title": "OAuth 2.0 bearer tokens for add-ons",
                    "url": "/cloud/jira/software/oauth-2-jwt-bearer-token-authorization-grant-type"
                  },
                  {
                    "title": "OAuth for REST APIs",
                    "url": "/cloud/jira/software/jira-rest-api-oauth-authentication"
                  },
                  {
                    "title": "Basic auth for REST APIs",
                    "url": "/cloud/jira/software/jira-rest-api-basic-authentication"
                  },
                  {
                    "title": "Cookie-based auth for REST APIs",
                    "url": "/cloud/jira/software/jira-rest-api-cookie-based-authentication"
                  }
                ],
                "url": "/cloud/jira/software/authentication-for-add-ons"
              }
            ]
          },
          {
            "name": "guides",
            "title": "Learning",
            "expandAlways": true,
            "items": [
              {
                "title": "Patterns and examples",
                "url": "/cloud/jira/software/patterns-and-examples/"
              },
              {
                "title": "Tutorials and guides",
                "id": "tutorialsAndGuides",
                "url": "/cloud/jira/software/tutorials-and-guides/"
              },
              {
                "title": "Storing data without a database",
                "url": "/cloud/jira/software/storing-data-without-a-database"
              }
            ]
          },
          {
            "name": "blocks",
            "title": "Building blocks",
            "expandAlways": true,
            "items": [
              {
                "title": "Add-on descriptor",
                "url": "/cloud/jira/software/add-on-descriptor"
              },
              {
                "title": "Conditions",
                "url": "/cloud/jira/software/conditions"
              },
              {
                "title": "Context parameters",
                "url": "/cloud/jira/software/context-parameters"
              },
              {
                "title": "Entity properties",
                "url": "/cloud/jira/software/jira-entity-properties"
              },
              {
                "title": "Scopes",
                "url": "/cloud/jira/software/scopes"
              },
              {
                "title": "Webhooks",
                "url": "/cloud/jira/software/webhooks"
              }
            ]
          },
          {
            "name": "other",
            "title": "Other considerations",
            "items": [
              {
                "title": "Atlassian Design Guidelines",
                "url": "https://design.atlassian.com/"
              },
              {
                "title": "Atlassian UI library",
                "url": "https://docs.atlassian.com/aui/"
              },
              {
                "title": "Developing for Marketplace",
                "url": "https://developer.atlassian.com/market/developing-for-the-marketplace"
              },
              {
                "title": "Public licensing",
                "url": "https://developer.atlassian.com/market/add-on-licensing-for-developers"
              }
            ]
          }
        ]
      },
      {
        "name": "reference",
        "title": "Reference",
        "url": "/cloud/jira/software/jira-software-cloud-rest-api",
        "subcategories": [
          {
            "name": "rest",
            "title": "REST APIS",
            "expandAlways": true,
            "items": [
              {
                "title": "About the JIRA Software Cloud REST API",
                "url": "/cloud/jira/software/jira-software-cloud-rest-api"
              },
              {
                "title": "JIRA Cloud platform REST API",
                "url": "https://docs.atlassian.com/jira/REST/cloud/"
              },
              {
                "title": "JIRA Cloud platform REST API scopes",
                "url": "/cloud/jira/software/jira-rest-api-scopes"
              },
              {
                "title": "JIRA Software Cloud REST API",
                "url": "https://docs.atlassian.com/jira-software/REST/cloud/"
              },
              {
                "title": "JIRA Software REST API scopes",
                "url": "/cloud/jira/software/jira-software-rest-api-scopes"
              }
            ]
          },
          {
            "name": "modules",
            "title": "Modules",
            "items": [
              {
                "title": "About JIRA modules",
                "url": "/cloud/jira/software/about-jira-modules"
              },
              {
                "title": "Boards",
                "url": "/cloud/jira/software/boards"
              },
              {
                "title": "Dashboard item",
                "url": "#"
              },
              {
                "title": "Dialog",
                "url": "#"
              },
              {
                "title": "Entity property",
                "url": "#"
              },
              {
                "title": "Global permission",
                "url": "#"
              },
              {
                "title": "Issue field",
                "url": "#"
              },
              {
                "title": "Page",
                "url": "#"
              },
              {
                "title": "Project admin tab panel",
                "url": "#"
              },
              {
                "title": "Project permission",
                "url": "#"
              },
              {
                "title": "Report",
                "url": "#"
              },
              {
                "title": "Search request view",
                "url": "#"
              },
              {
                "title": "Tab panel",
                "url": "#"
              },
              {
                "title": "Web item",
                "url": "#"
              },
              {
                "title": "Web panel",
                "url": "#"
              },
              {
                "title": "Web Section",
                "url": "#"
              },
              {
                "title": "Workflow post function",
                "url": "#"
              },
              {
                "title": "Extension points for the admin console",
                "url": "/cloud/jira/software/extension-points-for-the-admin-console"
              },
              {
                "title": "Extension points for project configuration",
                "url": "/cloud/jira/software/extension-points-for-project-configuration"
              },
              {
                "title": "Extension points for the end-user UI",
                "url": "/cloud/jira/software/extension-points-for-the-end-user-ui"
              },
              {
                "title": "Extension points for the 'View issue' page",
                "url": "/cloud/jira/software/extension-points-for-the-view-issue-page"
              }
            ]
          },
          {
            "name": "jsapi",
            "title": "JavaScript API",
            "items": [
              {
                "title": "About the JavaScript API",
                "url": "/cloud/jira/software/about-the-javascript-api"
              },
              {
                "title": "AP",
                "url": "#"
              },
              {
                "title": "Confluence",
                "url": "#"
              },
              {
                "title": "Cookie",
                "url": "#"
              },
              {
                "title": "Custom-content",
                "url": "#"
              },
              {
                "title": "Dialog",
                "url": "#"
              },
              {
                "title": "Events",
                "url": "#"
              },
              {
                "title": "History",
                "url": "#"
              },
              {
                "title": "Inline-dialog",
                "url": "#"
              },
              {
                "title": "JIRA",
                "url": "#"
              },
              {
                "title": "Messages",
                "url": "#"
              },
              {
                "title": "Navigator",
                "url": "#"
              },
              {
                "title": "Request",
                "url": "#"
              },
              {
                "title": "User",
                "url": "#"
              }
            ]
          }
        ]
      },
      {
        "title": "Get help",
        "name": "help",
        "url": "/cloud/jira/software/get-help"
      }
    ]
  }
}
