
# Change notifications

These developer documentation pages are updated frequently. To ensure you don't miss out on any changes, please join to the [Atlassian Developer Community](https://community.developer.atlassian.com/) or follow the changes using [API Changelog](https://www.apichangelog.com/api/atlassian):

<div class="apichangelog-widget">
    <script type="text/javascript" src="//www.apichangelog.com/static/widget/follow.js" api="atlassian"></script>
</div>