---
title: Architecture overview
platform: cloud
product: jswcloud
category: devguide
subcategory: intro
date: "2016-11-02"
---
{{< include path="docs/content/cloud/connect/concepts/cloud-development.snippet.md">}}