---
title: JIRA Software REST API scopes
platform: cloud
product: jswcloud
category: reference
subcategory: api
date: "2016-11-02"
---
# JIRA Software REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/jira-software-rest-api-scopes-reference.snippet.md" >}}