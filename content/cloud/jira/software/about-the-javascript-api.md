---
title: About the JavaScript API
platform: cloud
product: jswcloud
category: reference
subcategory: jsapi
date: "2017-02-15"
---
{{< include path="docs/content/cloud/connect/concepts/javascript-api.snippet.md">}}