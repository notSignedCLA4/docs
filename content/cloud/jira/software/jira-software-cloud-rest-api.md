---
title: JIRA Software Cloud REST API 
platform: cloud
product: jswcloud
category: reference
subcategory: api
aliases:
    - /jiracloud/jira-software-cloud-rest-api-39988028.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988028
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988028
confluence_id: 39988028
date: "2016-05-23"
---
# JIRA Software Cloud REST API

The JIRA REST APIs are used to interact with the JIRA Cloud applications remotely, for example, when building Connect add-ons or configuring webhooks. JIRA Software Cloud provides a REST API for application-specific features, like boards and sprints. Read the reference documentation below to get started.

[JIRA Software Cloud REST API] 

If you haven't used the JIRA REST APIs before, make sure you read the [Atlassian REST API policy]. 

## JIRA Cloud platform REST API

JIRA Service Desk is built on the JIRA platform. The JIRA Cloud platform provides a REST API for common features, like issues and workflows. 

[JIRA Cloud platform REST API]

## Authentication and authorization

-	Authentication: If you are building an Atlassian Connect add-on, [authentication (JWT-based)] is handled by the Atlassian Connect libraries. If you are calling the REST APIs directly, the following authentication methods are supported: [OAuth 1.0a], [basic authentication], [cookie-based authentication].

-	Authorization: If you are building an Atlassian Connect add-on, authorization is handled by [scopes] and add-on users, or by [exchanging a JWT for an OAuth 2.0 access token]. If you are calling the REST APIs directly, authorization is based on the user used in the authentication process.

For more information on authentication and authorization, read the [Security overview].

  [JIRA Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [Atlassian REST API policy]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy
  [JIRA Cloud platform REST API]: /cloud/jira/platform/jira-cloud-platform-rest-api
  [scopes]: /cloud/jira/software/jira-software-rest-api-scopes
  [authentication (JWT-based)]: /cloud/jira/software/authentication-for-add-ons
  [basic authentication]: /cloud/jira/software/jira-rest-api-basic-authentication
  [cookie-based authentication]: /cloud/jira/software/jira-rest-api-cookie-based-authentication
  [OAuth 1.0a]: /cloud/jira/software/jira-rest-api-oauth-authentication
  [exchanging a JWT for an OAuth 2.0 access token]: /cloud/jira/software/oauth-2-jwt-bearer-token-authorization-grant-type
  [Security overview]: /cloud/jira/software/security-overview
