---
title: "Cookie-based auth for REST APIs"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2016-11-02"
---
{{< reuse-page path="docs/content/cloud/jira/platform/jira-rest-api-cookie-based-authentication.md">}}