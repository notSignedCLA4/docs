---
title: JIRA Cloud platform REST API scopes
platform: cloud
product: jsdcloud
category: reference
subcategory: api
date: "2016-10-31"
---
# JIRA Cloud platform REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/jira-rest-api-scopes-reference.snippet.md" >}}